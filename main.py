import uvicorn
from typing import Union
from fastapi import FastAPI, Request, Response

# -----
# local app
from kvstore.storage import Storage

app = FastAPI()


# -----------------------------------------------------------------------------
# local functions
# -----------------------------------------------------------------------------

def client_ip(request: Request) -> str:
    print (request.headers)
    if "HTTP_X_FORWARDED_FOR" in request.headers and len(required.headers["HTTP_X_FORWARDED_FOR"]):
        return request.headers["HTTP_X_FORWARDED_FOR"]

    return request.client.host

# -----------------------------------------------------------------------------
#
# Todo: add authentication decorator
@app.get("/")
async def root():
    return {"message": "Hello FastAPI"}



@app.get("/storage/ip/{name}")
async def ip_get(name: str, q: Union[str, None] = None) -> dict:
    if q is not None and q == "raw":
        return Response(content=db_ip.get(name, "undef"), media_type="text/plain")

    return db_ip.get_json(name, "undef")


@app.get("/storage/ip")
async def ip_dump() -> dict:
    return db_ip.dumps({})


@app.get("/storage/ip/{name}/{value}")
async def ip_set(name: str, value: str, request: Request) -> dict:
    db_ip.set(name, value, client_ip(request))
    return db_ip.get_json(name, "error")

@app.get("/myip")
async def get_myip(request: Request) -> dict:
    return Response(content=client_ip(request), media_type="text/plain")


# ----------------------------------------------------------------------------
# Main
# ----------------------------------------------------------------------------
if __name__ == "__main__":
    db_ip = Storage(name="ip", db_dir="/tmp/db")
    uvicorn.run(app, port=8080, host="0.0.0.0")
