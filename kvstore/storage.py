#
# Store in db_dir/<db>/key
#
import os
import json
import time


class Storage:
    def __init__(self, db_dir: str, name: str):
        self.dbdir = db_dir
        self.name = name
        self.db = os.path.join(db_dir, name)
        self.configdir = os.path.join(self.db, ".config")

    def _keyfilename(self, key: str) -> str:
        # todo: remove path traversale
        return os.path.join(self.db, key)

    def _metafilename(self, key: str) -> str:
        # todo: remove path traversale
        return os.path.join(self.db, ".meta", key)

    def get_config(self, filename: str, default: str) -> str:
        # todo: remove path traversale
        try:
            return open(os.path.join(self.configdir, filename), "r").read()
        except:
            return default

    def get_modif(self, key) -> str:
        return time.ctime(os.path.getmtime(self._keyfilename(key)))

    def get(self, key: str, default: str) -> str:
        try:
            return open(self._keyfilename(key), "r").read().rstrip()
        except:
            return default

    def set(self, key: str, value: str, author: str):
        with open(self._keyfilename(key), "w") as f:
            f.write(value)
            f.close()
        with open(self._metafilename(key), "w") as f:
            f.write(json.dumps({"date:": time.ctime(), "author": author}))
            f.close()

    def get_json(self, key, default) -> dict:
        data = {}
        try:
            metas = json.load(open(self._metafilename(key), "r"))
            data = {
                key: self.get(key, default).rstrip(),
                "modif": self.get_modif(key),
                "by": metas["author"],
            }
        except Exception as e:
            import traceback

            print("".join(traceback.format_exception(e)))
            pass

        return data

    def dumps(self, default: str) -> dict:
        data = {self.name: []}
        try:
            for k in self.get_keys():
                data[self.name].append(self.get_json(k, ""))
        except Exception as e:
            import traceback

            print("".join(traceback.format_exception(e)))
            data = default

        return data

    def get_keys(self) -> list:
        return (k for k in os.listdir(self.db) if not k.startswith("."))

    def unset(db, key: str) -> None:
        # todo: remove pass traversal
        try:
            os.unlink(self._keyfilename(key))
        except:
            pass
